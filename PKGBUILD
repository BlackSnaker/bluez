pkgbase=bluez
pkgname=('bluez' 'bluez-utils' 'bluez-libs' 'bluez-cups' 'bluez-deprecated-tools' 'bluez-hid2hci' 'bluez-mesh' 'bluez-plugins')
pkgver=5.72
pkgrel=3
url="http://www.bluez.org/"
arch=('x86_64')
license=('GPL-2.0-only')
makedepends=('dbus' 'libical' 'systemd' 'alsa-lib' 'json-c' 'ell' 'python-docutils' 'python-pygments' 'cups')
source=(https://www.kernel.org/pub/linux/bluetooth/${pkgname}-${pkgver}.tar.xz
        bluetooth.modprobe
        0001-redhat-power-state-adapter-property.patch # Fedora: Remove experimental flag for PowerState
        AVRCP_TG_MDI_BV-04-C.patch         # SteamOS: For Bluetooth AVRCP certification test AVRCP/TG/MDI/BV-04-C, which requires a valid response from the get_element_attributes command.
        0002-valve-bluetooth-config.patch  # SteamOS: Enable compatibility with devices like AirPods Pro
        0003-valve-bluetooth-phy.patch     # SteamOS: Enable the phy # No longerneeded with kernel commit 288c90224eec55d13e786844b7954ef060752089, circa linux 6.4
        0004-shared-gatt-Prevent-security-level-change-for-PTS-GA.patch # SteamOS: For Bluetooth qualification tests GATT/CL/GAR/BI-{04,10,11,16}-C and GATT/CL/GAW/BI-{05,12}-C # ETA 5.73
        0005-btgatt-client-Add-command-to-prevent-security-level-.patch # SteamOS: For Bluetooth qualification tests GATT/CL/GAR/BI-{04,10,11,16}-C and GATT/CL/GAW/BI-{05,12}-C # ETA 5.73
        0006-btgatt-client-Add-function-to-search-service-based-o.patch # SteamOS: For Bluetooth qualification test GATT/CL/GAD/BV-02-C # ETA 5.73
        0007-btgatt-client-Add-function-to-search-characteristics.patch # SteamOS: For Bluetooth qualification tests GATT/CL/GAD/BV-05-C, GATT/CL/GAR/BV-03-C and GATT/CL/GAR/BI-{06,07,09,10,11}-C # ETA 5.73
        0008-btgatt-client-Add-function-to-search-all-primary-ser.patch # SteamOS: For Bluetooth qualification test GATT/CL/GAD/BV-01-C # ETA 5.73
)
# see https://www.kernel.org/pub/linux/bluetooth/sha256sums.asc
sha256sums=('499d7fa345a996c1bb650f5c6749e1d929111fa6ece0be0e98687fee6124536e'
            '46c021be659c9a1c4e55afd04df0c059af1f3d98a96338236412e449bf7477b4'
            '29434ed573219d70215ecc0cdb4322a1dccf38451c6225b8f0701240861f001b'
            'd863bd52917e4f5819b23ae5e64a34c5b02a0cfdf3969290bfce0d26dfe137b4'
            '42ca8090a4b04854210c7b3a4618e5bb09457247993151549b4db2c9109dacc6'
            '5d291d833c234a14b6162e3cb14eeff41505f5c3bb7c74528b65cb10597f39cb'
            'bd158dae9daf35686b3d8e97699feebe64cfe2926c4f03f74204c58fffcc0e37'
            '54606dd0d2a6e44d923c0afc168115c525d90da6e5eeb29c8b275fa8266437a6'
            '47de2881275c971e2a99b1cd50207cda8883ddbb44664f89fe387df7b1ee0e8a'
            '21bba701633e5cc53a18cd6edbbb503c7b6d135464820aa62bf7b827b9c94f3b'
            'd47837bb85e5b1faa62f4572cbe8e979a04cc6a10d5fb17b7b46b977e4595f33')

prepare() {
  cd "${pkgname}"-${pkgver}

    for patch in "${source[@]}"; do
    if [[ $patch == *.patch ]]; then
      echo "applying $patch"
      patch -Np1 -i "../$patch"
    fi
  done
}

build() {
  cd "${pkgname}"-${pkgver}
  ./configure \
          --prefix=/usr \
          --mandir=/usr/share/man \
          --sysconfdir=/etc \
          --localstatedir=/var \
          --libexecdir=/usr/lib \
          --with-dbusconfdir=/usr/share \
          --enable-btpclient \
          --enable-midi \
          --enable-sixaxis \
          --enable-mesh \
          --enable-hid2hci \
          --enable-experimental \
          --enable-datafiles \
          --enable-library --enable-deprecated # libraries and these tools are deprecated
  make

  # fake installation to be seperated into packages
  make DESTDIR="${srcdir}/fakeinstall" install

  # add missing tools FS#41132, FS#41687, FS#42716
  for files in `find tools/ -type f -perm -755`; do
    filename=$(basename $files)
    install -Dm755 "${srcdir}"/"${pkgbase}"-${pkgver}/tools/$filename "${srcdir}/fakeinstall"/usr/bin/$filename
  done
}

_install() {
  local src f dir
  for src; do
    f="${src#fakeinstall/}"
    dir="${pkgdir}/${f%/*}"
    install -m755 -d "${dir}"
    # use copy so a new file is created and fakeroot can track properties such as setuid
    cp -av "${src}" "${dir}/"
    rm -rf "${src}"
  done
}

check() {
  cd "$pkgname"-$pkgver
  # fails test-vcp due to lto - https://github.com/bluez/bluez/issues/683
  make check || /bin/true
}


package_bluez() {
  pkgdesc="Daemons for the bluetooth protocol stack"
  depends=('systemd' 'dbus' 'glib2' 'alsa-lib' 'glibc' 'readline' 'libical')
  backup=(etc/bluetooth/{main,input,network}.conf)

  _install fakeinstall/usr/lib/bluetooth/bluetoothd
  _install fakeinstall/usr/lib/systemd/system/bluetooth.service
  _install fakeinstall/usr/share/dbus-1/system-services/org.bluez.service
  _install fakeinstall/usr/share/dbus-1/system.d/bluetooth.conf
  _install fakeinstall/usr/share/man/man8/bluetoothd.8

  # bluez-obex
  _install fakeinstall/usr/bin/{obexctl,obex-client-tool,obex-server-tool}
  _install fakeinstall/usr/lib/bluetooth/obexd
  _install fakeinstall/usr/lib/systemd/user/obex.service
  _install fakeinstall/usr/share/dbus-1/services/org.bluez.obex.service
  _install fakeinstall/usr/share/man/man5/org.bluez.obex*.5

  # ship upstream main config files
  install -dm555 "${pkgdir}"/etc/bluetooth
  install -Dm644 "${srcdir}"/"${pkgbase}"-${pkgver}/src/main.conf "${pkgdir}"/etc/bluetooth/main.conf
  install -Dm644 "${srcdir}"/"${pkgbase}"-${pkgver}/profiles/input/input.conf "${pkgdir}"/etc/bluetooth/input.conf
  install -Dm644 "${srcdir}"/"${pkgbase}"-${pkgver}/profiles/network/network.conf "${pkgdir}"/etc/bluetooth/network.conf

  # add basic documention
  install -dm755 "${pkgdir}"/usr/share/doc/"${pkgbase}"/dbus-apis
  cp -a "${pkgbase}"-${pkgver}/doc/*.txt "${pkgdir}"/usr/share/doc/"${pkgbase}"/dbus-apis/
  # fix module loading errors
  install -dm755 "${pkgdir}"/usr/lib/modprobe.d
  install -Dm644 "${srcdir}"/bluetooth.modprobe "${pkgdir}"/usr/lib/modprobe.d/bluetooth-usb.conf
  # load module at system start required by some functions
  # https://bugzilla.kernel.org/show_bug.cgi?id=196621
  install -dm755 "$pkgdir"/usr/lib/modules-load.d
  echo "crypto_user" > "$pkgdir"/usr/lib/modules-load.d/bluez.conf

  # fix obex file transfer - https://bugs.archlinux.org/task/45816
  ln -fs /usr/lib/systemd/user/obex.service "${pkgdir}"/usr/lib/systemd/user/dbus-org.bluez.obex.service
}

package_bluez-utils() {
  pkgdesc="Development and debugging utilities for the bluetooth protocol stack"
  depends=('dbus' 'systemd-libs' 'glib2' 'glibc' 'readline')
  optdepends=('ell: for btpclient')

  _install fakeinstall/usr/bin/{advtest,amptest,avinfo,avtest,bcmfw,bdaddr,bluemoon,bluetoothctl,bluetooth-player,bneptest,btattach,btconfig,btgatt-client,btgatt-server,btinfo,btiotest,btmgmt,btmon,btpclient,btpclientctl,btproxy,btsnoop,check-selftest,cltest,create-image,eddystone,gatt-service,hcieventmask,hcisecfilter,hex2hcd,hid2hci,hwdb,ibeacon,isotest,l2ping,l2test,mcaptest,mpris-proxy,nokfw,oobtest,rctest,rtlfw,scotest,seq2bseq,test-runner}
  _install fakeinstall/usr/share/man/man1/bluetoothctl*.1
  _install fakeinstall/usr/share/man/man1/{btattach,btmgmt,btmon,isotest,l2ping,rctest}.1
  _install fakeinstall/usr/share/man/man5/org.bluez.{A,B,D,G,I,L,M,N,P}*.5
  _install fakeinstall/usr/share/zsh/site-functions/_bluetoothctl
}

package_bluez-deprecated-tools() {
  pkgdesc="Deprecated tools that are no longer maintained"
  depends=('json-c' 'systemd-libs' 'glib2' 'dbus' 'readline' 'glibc')

  _install fakeinstall/usr/bin/{ciptool,hciattach,hciconfig,hcidump,hcitool,meshctl,rfcomm,sdptool}
  _install fakeinstall/usr/share/man/man1/{ciptool,hciattach,hciconfig,hcidump,hcitool,rfcomm,sdptool}.1
}

package_bluez-libs() {
  pkgdesc="Deprecated libraries for the bluetooth protocol stack"
  depends=('glibc')
  provides=('libbluetooth.so')
  license=('LGPL-2.1-only')

  _install fakeinstall/usr/include/bluetooth/*
  _install fakeinstall/usr/lib/libbluetooth.so*
  _install fakeinstall/usr/lib/pkgconfig/*
}

package_bluez-cups() {
  pkgdesc="CUPS printer backend for Bluetooth printers"
  depends=('cups' 'glib2' 'glibc' 'dbus')

  _install fakeinstall/usr/lib/cups/backend/bluetooth
}

package_bluez-hid2hci() {
  pkgdesc="Put HID proxying bluetooth HCI's into HCI mode"
  depends=('systemd-libs' 'glibc')

  _install fakeinstall/usr/lib/udev/*
  _install fakeinstall/usr/share/man/man1/hid2hci.1
}

package_bluez-mesh() {
  pkgdesc="Services for bluetooth mesh"
  depends=('systemd' 'json-c' 'readline' 'glibc')
  backup=('etc/bluetooth/mesh-main.conf')

  _install fakeinstall/usr/bin/{mesh-cfgclient,mesh-cfgtest}
  _install fakeinstall/usr/lib/bluetooth/bluetooth-meshd
  _install fakeinstall/usr/lib/systemd/system/bluetooth-mesh.service
  _install fakeinstall/usr/share/dbus-1/system-services/org.bluez.mesh.service
  _install fakeinstall/usr/share/dbus-1/system.d/bluetooth-mesh.conf
  _install fakeinstall/usr/share/man/man8/bluetooth-meshd.8

  # ship upstream mesh config file
  install -dm555 "${pkgdir}"/etc/bluetooth
  install -Dm644 "${srcdir}"/"${pkgbase}"-${pkgver}/mesh/mesh-main.conf "${pkgdir}"/etc/bluetooth/mesh-main.conf
}

package_bluez-plugins() {
  pkgdesc="bluez plugins (PS3 Sixaxis controller)"
  depends=('systemd-libs' 'glibc')

  _install fakeinstall/usr/lib/bluetooth/plugins/sixaxis.so

  # make sure there are no files left to install
  rm fakeinstall/usr/lib/libbluetooth.la
  rm fakeinstall/usr/lib/bluetooth/plugins/sixaxis.la
  find fakeinstall -depth -print0 | xargs -0 rmdir
}
